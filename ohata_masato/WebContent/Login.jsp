<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
	    <link href="./css/login.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ログイン</title>
    </head>
    <body>
        <div class="main-contents">
            <c:if test="${ not empty loginMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${loginMessages}" var="loginmessage">
                            <li><c:out value="${loginmessage}"/>
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="loginMessages" scope="session"/>
            </c:if>

            <form action="login" method="post"><br />
               <p class="form-title">業務連絡掲示板</p>
               <p><label for="login_id">ログインID</label>
                <input name="login_id" id="login_id"value="${login_id}" /> <br ></p>
                <p class="pass"><label for="password">パスワード</label>
                <input name="password" type="password" id="password" /> <br /></p>

                <p class="submit"><input type="submit" value="ログイン" /> <br /></p>
            </form>
        </div>
    </body>
      <div class="copyright"> Copyright(c)masato ohata</div>
</html>