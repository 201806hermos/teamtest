<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	 <link href="./css/signup.css" rel="stylesheet" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>編集画面</title>
</head>
   <body>
        <div class="main-contents">
          <c:if test="${ not empty editmessages }">
                <div class="editmessages">
                    <ul>
                        <c:forEach items="${editmessages}" var="message">
                            <li><c:out value="${message}" /></li>
                        </c:forEach>
                    </ul>
                    <c:remove var="editmessages" scope="session"/>
                </div>
            </c:if>

            <form action= "Edit" method="post"><br />
                <ul>
                	<li><input name="id" value="${editUser.id}" id="id" type="hidden"/>
                		<label for="name">名前</label>
                		<input name="name" value="${editUser.name}" id="name"/><br /></li>

                	<li><label for="login_id">ログインID</label>
                		<input name="login_id" value="${editUser.login_id}" /><br /></li>

                	<li><label for="password">パスワード</label>
                		<input name="password" type="password" id="password"/> <br /></li>

	                <li><label for="passwordconfirm">パスワード確認用</label>
                		<input name="passwordconfirm" type="password" id="passwordconfirm"/> <br /></li>

				<c:if test="${editUser.id ==loginUser.id }">
					<input type="hidden"  name="branch_id" value="${editUser.branch_id}" />
					<input type="hidden"  name="department_id" value="${editUser.department_id}" />
				</c:if>


				<c:if test="${editUser.id !=loginUser.id }">
				<label for="branch_id">支店</label>
				<select name="branch_id">
                    <c:forEach items="${branchName}" var="branch">
                        <c:if test="${branch.branch_id eq editUser.branch_id}">
                            <option id="branch_id" value="${branch.branch_id}" selected>${branch.branch_name}</option>
                        </c:if>
                        <c:if test="${branch.branch_id ne editUser.branch_id}">
                            <option id="branch_id" value="${branch.branch_id}">${branch.branch_name}</option>
                        </c:if>
                   </c:forEach>
                </select><br />
                </c:if>

				<c:if test="${editUser.id !=loginUser.id }">
                <label for="department_id">部署・役職</label>
                <select name="department_id">
					<c:forEach items="${departmentName}" var="department">
                        <c:if test="${department.department_id eq editUser.department_id}">
                            <option id="department_id" value="${department.department_id}" selected>${department.department_name}</option>
                        </c:if>
                        <c:if test="${department.department_id ne editUser.department_id}">
                            <option id="department_id" value="${department.department_id}">${department.department_name}</option>
                        </c:if>
                     </c:forEach>
                </select>
                </c:if>

				<input type="hidden" name="user_id" value="${editUser.id}" /><br />
            	<li><input type="submit" value="登録" /> <br /></li>
            			<br />
            			<br />
            			<br />
					<a href ="Management"> ユーザー管理画面に戻る</a></li>
				</ul>
            </form>
        </div>
        <div class="copyright"> Copyright(c)masato ohata</div>
</html>