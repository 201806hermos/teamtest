package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import exceptions.SQLRuntimeException;

public class DeleteDao {
	public void delete (Connection connection, int deletecontribute) {
		PreparedStatement ps = null;

		try{
			StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM");
            sql.append(" CONTRIBUTIONS");
            sql.append(" WHERE");
            sql.append(" ID");
            sql.append(" =");
            sql.append(" ?");


            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, deletecontribute);
            ps.executeUpdate();

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally {
            close(ps);
        }
	}
	public void deletecomment(Connection connection, int deletecomment) {
		PreparedStatement ps = null;

		try{
			StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM");
            sql.append(" comments");
            sql.append(" WHERE");
            sql.append(" id");
            sql.append(" =");
            sql.append(" ?");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, deletecomment);
            ps.executeUpdate();
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally {
            close(ps);
        }
	}
}
