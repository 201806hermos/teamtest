package dao;
import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Usercontribute;
import exceptions.SQLRuntimeException;

public class UserContributeDao {
	    public List<Usercontribute> getUserContribute(Connection connection, String category, String fromday,String untillday, int num) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("SELECT ");
	            sql.append("contributions.id as id");
	            sql.append(", contributions.text as text");
	            sql.append(", contributions.subject as subject");
	            sql.append(", contributions.user_id as user_id");
	            sql.append(", contributions.created_date as created_date");
	            sql.append(", contributions.category as category");
	            sql.append(", users.name as name ");
	            sql.append("FROM contributions ");
	            sql.append("INNER JOIN users ");
	            sql.append("ON Contributions.user_id = users.id ");
	            sql.append("WHERE category LIKE ? ");
	            sql.append("AND contributions.created_date >= ? AND contributions.created_date < ? ORDER BY created_date DESC limit " + num);

	            if(StringUtils.isEmpty(category)){
		            ps = connection.prepareStatement(sql.toString());
		            ps.setString(1, "%"+""+"%");
		            ps.setString(2, fromday+" 00:00:00");
		            ps.setString(3, untillday+" 23:59:59");
	            }else if(!StringUtils.isEmpty(category)){
	            	ps = connection.prepareStatement(sql.toString());
		            ps.setString(1, "%"+category+"%");
		            ps.setString(2, fromday+" 00:00:00");
		            ps.setString(3, untillday+" 23:59:59");
	            }

	            ps.toString();
	            ResultSet rs = ps.executeQuery();

	            List<Usercontribute>ret = toUserMessageList(rs);
	            return ret;
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }

	    private List<Usercontribute> toUserMessageList(ResultSet rs)
	            throws SQLException {

	        List<Usercontribute> ret = new ArrayList<Usercontribute>();
	        try {
	            while (rs.next()) {
	                String name = rs.getString("name");
	                int id = rs.getInt("id");
	                int user_Id = rs.getInt("user_id");
	                String text = rs.getString("text");
	                String category = rs.getString("category");
	                String subject = rs.getString("subject");
	                Timestamp created_Date = rs.getTimestamp("created_date");

	                Usercontribute Contributes = new Usercontribute();
	                Contributes.setName(name);
	                Contributes.setId(id);
	                Contributes.setUser_Id(user_Id);
	                Contributes.setText(text);
	                Contributes.setCategory(category);
	                Contributes.setSubject(subject);
	                Contributes.setCreated_Date(created_Date);

	                ret.add(Contributes);
	            }
	            return ret;
	        } finally {
	            close(rs);
	    }
	}
}
