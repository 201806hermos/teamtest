package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import dao.DepartmentDao;

public class DepartmentService {

	public List<User> getPosition() {

		Connection connection = null;
        try {
            connection = getConnection();

            DepartmentDao positionDao = new DepartmentDao();
            List<User> ret = positionDao.getDepartmentPosition(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}
