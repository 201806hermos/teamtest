package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import dao.ManagementDao;

public class ManagementService {
	public List<User> getdata() {

		Connection connection = null;
	    try {
	    	connection = getConnection();
	        ManagementDao userdataDao = new ManagementDao();
	        List<User> UserList =userdataDao.getManagementList(connection);

	        commit(connection);
	        return UserList ;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
}