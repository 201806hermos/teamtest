package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.User;
import dao.UserDao;

public class AccountService {
	public void parameterChange(User user){

		Connection connection = null;
		try {
			connection=getConnection();

			UserDao accountDao = new UserDao();
			accountDao.accountChange(connection,user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
