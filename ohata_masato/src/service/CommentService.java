package service;
import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Comment;
import beans.UserComment;
import dao.CommentDao;
import dao.DeleteDao;
import dao.UserCommentDao;

public class CommentService{
	public void CommentsRegister(Comment comment) {
	Connection connection = null;
		try {
			connection = getConnection();
			CommentDao commentDao = new CommentDao();
			commentDao.insert(connection, comment);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	private static final int LIMIT_NUM = 500;
	public List<UserComment> getcomment() {
		Connection connection = null;
			try {
				connection = getConnection();
				UserCommentDao commentDao = new UserCommentDao();
				List<UserComment> ret = commentDao.getUserComment(connection, LIMIT_NUM);
				commit(connection);
				return ret;
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	    }
	}

	public void registerDelete(int deletecomment) {
		Connection connection = null;
		try {
			connection = getConnection();
			DeleteDao deleteDao = new DeleteDao();
			deleteDao.deletecomment(connection, deletecomment);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
