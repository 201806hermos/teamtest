package beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String login_Id;
    private String password;
    private String name;
    private int branch_id;
    private int department_id;
    private int is_deleted;
    private String branch_name;
    private String department_name;
    private Date created_Date;
    private Date updatedDate;
    private String category;
    private String fromday;
    private String untilday;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLogin_id( ) {
		return login_Id;
	}
	public void setLogin_id(String login_id) {
		this.login_Id = login_id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword (String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBranch_id() {
		return branch_id;
	}
	public void setBranch_id(int branch_id) {
		this.branch_id = branch_id;
	}
	public int getDepartment_id() {
		return department_id;
	}
	public void setDepartment_id(int department_id) {
		this.department_id = department_id;
	}
	public int getis_deleted() {
		return is_deleted;
	}
	public void setIs_deleted(int is_deleted) {
		this.is_deleted = is_deleted;
	}
	public Date getCreated_Date() {
		return created_Date;
	}
	public void setCreated_Date(Date createdDate) {
		this.created_Date = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getBranch_Name() {
		return branch_name;
	}
	public void setBranch_Name(String branch_Name) {
		branch_name = branch_Name;
	}
	public String getDepartment_Name() {
		return department_name;
	}
	public void setDepartment_Name(String department_Name) {
		department_name = department_Name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getFromday() {
		return fromday;
	}
	public void setFromday(String fromday) {
		this.fromday = fromday;
	}
	public String getUntilday() {
		return untilday;
	}
	public void setUntilday(String untilday) {
		this.untilday = untilday;
	}
}