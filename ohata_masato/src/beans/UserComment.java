package beans;

import java.io.Serializable;
import java.util.Date;

public class UserComment implements Serializable {
	private static final long serialVersionUID = 1L;

    private int id;
    private int users_Id;
    private int contributions_id;
    private String name;
    private String text;
    private Date created_date;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getUsers_Id() {
		return users_Id;
	}
	public void setUsers_Id(int users_Id) {
		this.users_Id = users_Id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_Date(Date created_date) {
		this.created_date = created_date;
	}
	public int getContributions_id() {
		return contributions_id;
	}
	public void setContributions_Id(int contributions_id) {
		this.contributions_id = contributions_id;
	}
}