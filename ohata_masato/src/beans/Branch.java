package beans;

import java.io.Serializable;

public class Branch implements Serializable {
	public static final long serialVersionUID = 1L;

	private int Branch_id;
	private String branch_name;

	public int getBranch_id() {
		return Branch_id;
	}
	public void setBranch_id(int branch_id) {
		Branch_id = branch_id;
	}
	public String getBranch_name() {
		return branch_name;
	}
	public void setBranch_name(String branch_name) {
		this.branch_name = branch_name;
	}




}
