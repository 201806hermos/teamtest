package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import service.ContributeService;

/**
 * Servlet implementation class ContributeDeleteServlet
 */
@WebServlet("/ContributeDeleteServlet")
public class ContributeDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    protected void doPost(HttpServletRequest request,
    HttpServletResponse response) throws IOException, ServletException {


		int deletecontribute=Integer.parseInt(request.getParameter("contribute_id"));
		ContributeService contributeservice =new ContributeService();
		contributeservice.registerDelete(deletecontribute);

		HttpSession session = request.getSession();
	        session.setAttribute("deletecontribute",deletecontribute);

	       String messages=("メッセージを削除しました");
	       session.setAttribute("messages", messages);

	       response.sendRedirect("./");

	}
}
