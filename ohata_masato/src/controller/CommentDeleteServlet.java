package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import service.CommentService;

/**
 * Servlet implementation class CommentDeleteServlet
 */
@WebServlet(urlPatterns = {"/CommentDeleteServlet"})
public class CommentDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    protected void doPost(HttpServletRequest request,
    HttpServletResponse response) throws IOException, ServletException {

		int deletecomment=Integer.parseInt(request.getParameter("comment_id"));
		CommentService commentservice =new CommentService();
		commentservice.registerDelete(deletecomment);

		HttpSession session = request.getSession();
	        session.setAttribute("deletecomment",deletecomment);

	       String commentMessage=("コメントを削除しました");
	       session.setAttribute("commentMessage", commentMessage);

	       response.sendRedirect("./");
	}
}

